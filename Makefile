IMAGE=registry.gitlab.com/biliyun/website
TAG?=latest
ifneq ($(img),)
    IMAGE = $(img)
endif
ifneq ($(tag),)
    TAG = $(tag)
endif
image: 
	# npm i --registry=https://registry.npm.taobao.org
	npm i
	npm run build
	docker build --no-cache -t $(IMAGE):$(TAG) .
push: image
	docker push $(IMAGE):$(TAG)
	docker rmi $(IMAGE):$(TAG)