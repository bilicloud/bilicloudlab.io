FROM nginx:latest
USER root
COPY ./dist /usr/share/nginx/html
COPY nginx /etc/nginx/conf.d
RUN ls -l /usr/share/nginx/html
RUN apt-get update && apt-get install -y curl
EXPOSE 8080
CMD ["nginx","-g","daemon off;"]

# FROM repo.treescale.com/acgame/nginx:latest
# WORKDIR /web
# USER root
# COPY package.json ./
# COPY ./ ./
# RUN npm i --registry=https://registry.npm.taobao.org
# # RUN yarn
# RUN npm run build
# RUN mv -f dist/* /usr/share/nginx/html
# COPY nginx /etc/nginx/conf.d
# RUN ls -l
# RUN rm -rf ./*
# EXPOSE 80
# CMD ["nginx","-g","daemon off;"]

# FROM repo.treescale.com/acgame/nginx:latest
# COPY package.json /acgames/package.json
# COPY postcss.config.js /acgames/postcss.config.js
# COPY .bootstraprc /acgames/.bootstraprc
# COPY .babelrc /acgames/.babelrc
# COPY src /acgames/src
# COPY build /acgames/build
# COPY nginx /etc/nginx/conf.d
# RUN npm i --registry=https://registry.npm.taobao.org
# ENV NODE_ENV=production
# RUN npm run build
# RUN mv -f dist/* /usr/share/nginx/html
# EXPOSE 80
# CMD ["nginx","-g","daemon off;"]

# FROM circleci/node:latest-browsers

# WORKDIR /usr/src/app/
# USER root
# COPY package.json ./
# RUN yarn

# COPY ./ ./

# RUN npm run test:all

# CMD ["npm", "run", "build"]